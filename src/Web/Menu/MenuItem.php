<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 2/28/15
 * Time: 10:11 PM
 */

namespace RCSI\Web\Menu;


class MenuItem {

    private $id;
    private $class;
    private $text;
    private $url;
    private $target;
    private $icon;
    private $children = array();

    public function __construct(array $array = null)
    {
        if($array !== null) {
            $this->setAll($array);
        }
    }


    public function toHTML($outer, $inner)
    {
        /**
         * @var $child MenuItem
         */
        $out  = "<{$inner} id='{$this->id}' class='{$this->class}'>";
        if ($this->notBlank($this->url)) {
            $out .= "<a href='" . $this->url . "' ";
            $out .= $this->notBlank($this->target) ? "target='{$this->target}' " :"";
            $out .= ">";
        }
        if ($this->notBlank($this->icon)) {
            $out .= $this->icon . " ";
        }
        $out .= $this->text;
        if ($this->notBlank($this->url)) {
            $out .= "</a>";
        }
        if (!empty($this->children)) {
            $out .= "\n\t<{$outer}>\n";
            foreach ($this->children as $child) {
                $out .= $child->toHTML($outer, $inner);
            }
            $out .= "\t</{$outer}>\n";
        }
        $out .= "</{$inner}>\n";
        return $out;
    }

    public function toBootstrap()
    {
        /**
         * @var $child MenuItem
         */
        $dropdown = !empty($this->children);
        $liClass = $dropdown ? "dropdown" : "";
        $out = "<li class='{$liClass} {$this->class}'>";
        if ($dropdown) {
            $out .= "<a href='#' class='dropdown-toggle' data-toggle='dropdown' ";
            $out .= "role='button' aria-expanded='false'>";
            $out .= $this->notBlank($this->icon) ? $this->icon . "&nbsp;" : "";
            $out .= $this->text;
            $out .= "<span class='caret'></span>";
            $out .= "</a>\n";
            $out .= "<ul class='dropdown-menu' role='menu'>";
            foreach ($this->children as $child) {
                $out .= $child->toBootstrap();
            }
            $out .= "</ul>\n";
        } elseif ($this->notBlank($this->url)) {
            $out .= "<a href='" . $this->url . "' ";
            $out .= $this->notBlank($this->target) ? "target='{$this->target}'" : "";
            $out .= ">";
            $out .= $this->notBlank($this->icon) ? $this->icon . "&nbsp;" : "";
            $out .= $this->text;
            $out .= "</a>\n";
        } else {
            $out .= "<a href='#'>";
            $out .= $this->notBlank($this->icon) ? $this->icon . "&nbsp;" : "";
            $out .= $this->text;
            $out .= "</a>\n";
        }

        $out .= "</li>\n";
        return $out;
    }

    public function toArray()
    {
        $out = array(
            'id' => $this->id,
            'class' => $this->class,
            'text' => $this->text,
            'url' => $this->url,
            'target' => $this->target,
            'icon' => $this->icon,
            'children' => $this->childrenArray(),
        );
        return $out;
    }

    private function notBlank($data)
    {
        if ($data !== null && $data !== '') {
            return true;
        }
        return false;
    }

    private function childrenArray()
    {
        /**
         * @var MenuItem $child
         */
        $out = array();
        if (empty($this->children)) {
            return $out;
        }
        foreach($this->children as $child)
        {
            if(is_array($child)) {
                $out[] = $child;
            } elseif (get_class($child) === get_class($this)) {
                $out[] = $child->toArray();
            }
        }
        return $out;
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }

    public function setAll(array $array)
    {
        foreach ($array as $key => $value) {
            switch ($key) {
                case "id":
                    $this->setID($value);
                    break;
                case "class":
                    $this->setClass($value);
                    break;
                case "url":
                    $this->setUrl($value);
                    break;
                case "text":
                    $this->setText($value);
                    break;
                case "target":
                    $this->setTarget($value);
                    break;
                case "icon":
                    $this->setIcon($value);
                    break;
                case "children":
                    $this->setChildren($value);
                    break;
            }
        }
        return $this;
    }

    /**
     * @param array|MenuItem $children
     * @return $this
     */
    public function setChildren($children)
    {
        if($children === null) {
            $this->children = array();
        } elseif (is_array($children)) {
            foreach ($children as $child) {
                if (is_array($child)) {
                    $newChild = new self();
                    $newChild->setAll($child);
                    $this->children[] = $newChild;
                } elseif (get_class($child) === get_class($this)) {
                    $this->children[] = $child;
                }
            }
        } elseif (get_class($children) === get_class($this)) {
            $this->children[] = $children;
        }
        return $this;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }


    /**
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $target
     * @return $this
     */
    public function setTarget($target)
    {
        $this->target = $target;
        return $this;
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string|null $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setID($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getID()
    {
        return $this->id;
    }

}
