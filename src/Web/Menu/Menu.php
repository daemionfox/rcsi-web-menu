<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 2/28/15
 * Time: 10:10 PM
 */

namespace RCSI\Web\Menu;

class Menu {

    private $brand;
    private $defaultLink = "#";
    private $menuArray = array();

    public function __construct(array $array = null)
    {
        if ($array !== null && is_array($array)) {
            foreach($array as $key=>$value){
                $this->menuArray[] = new MenuItem($value);
            }
        }
    }

    public function toList()
    {
        /**
         * @var $menuItem MenuItem
         */
        $out = "<ul>\n";
        foreach ($this->menuArray as $menuItem) {
            $out .= $menuItem->toHTML('ul', 'li');
        }
        $out .= "</ul>\n";
        return $out;
    }

    public function toDiv()
    {
        /**
         * @var $menuItem MenuItem
         */
        $out = "<div>\n";
        foreach ($this->menuArray as $menuItem) {
            $out .= $menuItem->toHTML('div', 'div');
        }
        $out .= "</div>\n";
        return $out;

    }

    public function toBootstrap($align = 'right')
    {
        $collapseAlign = '';
        $barAlign = '';
        switch (strtolower($align)) {
            case 'left':
                $collapseAlign = 'pull-right';
                $barAlign = 'pull-left';
                break;
            case 'right':
                $collapseAlign = 'pull-left';
                $barAlign = 'pull-right';
                break;
        }

        /**
         * @var $item MenuItem
         */

        $collapse = "<div class='navbar-header {$collapseAlign}'>\n";
        $collapse .= "<button type='button' class='navbar-toggle collapsed' ";
        $collapse .= "data-toggle='collapse' data-target='#navbar-collapse'>\n";
        $collapse .= "<span class='sr-only'>Toggle navigation</span>\n";
        $collapse .= "<span class='icon-bar'></span>\n";
        $collapse .= "<span class='icon-bar'></span>\n";
        $collapse .= "<span class='icon-bar'></span>\n";
        $collapse .= "</button>\n";
        $collapse .= $this->brand !== null ?
            "<a class='navbar-brand' href='{$this->defaultLink}'>{$this->brand}</a>\n" :
            "";
        $collapse .= "</div>\n";

        $bar = "<div class='collapse navbar-collapse {$barAlign}' id='navbar-collapse'>\n";
        $bar .= $this->bootstrapList();
        $bar .= "</div>\n"; //.navbar-collapse


        $out  = "<nav class='navbar navbar-default' role='navigation'>\n";
        $out .= "<div class='container-fluid'>\n";

        if (strtolower($align) === 'left') {
            $out .= $bar;
            $out .= $collapse;
        } else {
            $out .= $collapse;
            $out .= $bar;
        }

        $out .= "</div>\n"; //.container-fluid
        $out .= "</nav>\n";
        return $out;
    }

    public function bootstrapList()
    {
        /**
         * @var $item MenuItem
         */
        $out = "<ul class='nav navbar-nav'>\n";
        foreach ($this->menuArray as $item) {
            $out .= $item->toBootstrap();
        }
        $out .= "</ul>\n";
        return $out;
    }

    public function toArray()
    {
        /**
         * @var $item MenuItem
         */
        $out = array();
        foreach($this->menuArray as $item)
        {
            $out[] = $item->toArray();
        }
        return $out;
    }

    public function toJSON()
    {
        return json_encode($this->toArray(), JSON_PRETTY_PRINT);
    }

    /**
     * @return string
     */
    public function getDefaultLink()
    {
        return $this->defaultLink;
    }

    /**
     * @param string $defaultLink
     * @return $this
     */
    public function setDefaultLink($defaultLink)
    {
        $this->defaultLink = $defaultLink;
        return $this;
    }

    public function dump()
    {
        return print_r($this->menuArray, true);
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }


} 
